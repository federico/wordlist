use std::cmp::Ordering;

const BLOB: &'static str = include_str!("peter-broda-wordlist__scored.txt");

const MAX_WORD_SCORE: u8 = 100;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
struct PlainWord {
    start: usize,
    len: usize,
    score: u8,
}

impl PlainWord {
    fn as_str<'a>(&self, blob: &'a str) -> &'a str {
        &blob[self.start..self.start + self.len]
    }
}

#[derive(Debug, PartialEq)]
struct UnsortedWords {
    blob: String,
    words: Vec<PlainWord>,
}

impl UnsortedWords {
    fn from_blob(blob: &str) -> UnsortedWords {
        let lines: Vec<Line> = blob
            .lines()
            .map(|s| parse_line(s))
            .filter(|line_result| line_result.is_ok())
            .map(Result::unwrap)
            .collect();

        let (_offset, unsorted_words) = lines.iter().fold(
            // Accumulator is a pair of (offset, UnsortedWords)
            (
                0,
                UnsortedWords {
                    blob: String::new(),
                    words: Vec::new(),
                },
            ),
            |acc, line| {
                let (
                    offset,
                    UnsortedWords {
                        mut blob,
                        mut words,
                    },
                ) = acc;

                blob.push_str(line.word);
                words.push(PlainWord {
                    start: offset,
                    len: line.word.len(),
                    score: line.score,
                });

                (offset + line.word.len(), UnsortedWords { blob, words })
            },
        );

        unsorted_words
    }
}

#[derive(Debug, PartialEq)]
enum LineParseError {
    Invalid,
    DisallowedChar { position: usize },
    InvalidScore,
}

#[derive(Debug, PartialEq)]
struct Line<'a> {
    word: &'a str,
    score: u8,
}

fn is_char_allowed(ch: char) -> bool {
    ch.is_ascii_uppercase()
}

fn parse_line(line: &str) -> Result<Line, LineParseError> {
    // Get the word and the score substrings

    let mut parts = line.split(';');

    let word = parts
        .next()
        .expect("split() always yields at least one item with the empty string");

    let score_str = parts.next().ok_or_else(|| LineParseError::Invalid)?;

    if parts.next().is_some() {
        return Err(LineParseError::Invalid);
    }

    // Validate the word

    if word.is_empty() {
        return Err(LineParseError::Invalid);
    }

    for (i, ch) in word.chars().enumerate() {
        if !is_char_allowed(ch) {
            return Err(LineParseError::DisallowedChar { position: i });
        }
    }

    // Validate the score

    let score: u8 = score_str
        .parse()
        .map_err(|_| LineParseError::InvalidScore)?;

    if score > MAX_WORD_SCORE {
        return Err(LineParseError::InvalidScore);
    }

    // Done

    Ok(Line { word, score })
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Word {
    id: u32,
    start: u32,
    len: u8,
    score: u8,
}

impl Word {
    fn as_str<'a>(&self, blob: &'a str) -> &'a str {
        let start = self.start as usize;
        let len = self.len as usize;
        &blob[start..start + len]
    }
}

#[derive(Debug, PartialEq)]
struct Words {
    blob: String,
    words: Vec<Word>,
}

impl Word {
    fn new(id: u32, start: usize, len: usize, score: u8) -> Word {
        assert!(start <= u32::MAX as usize);
        assert!(len <= u8::MAX as usize);

        Word {
            id,
            start: start as u32,
            len: len as u8,
            score,
        }
    }
}

fn compare_plain_words(blob: &str, a: &PlainWord, b: &PlainWord) -> Ordering {
    if a.len != b.len {
        // Primary key is the length
        a.len.cmp(&b.len)
    } else {
        // Secondary key is the word itself
        let wa = a.as_str(blob);
        let wb = b.as_str(blob);

        wa.cmp(wb)
    }
}

impl Words {
    fn from_unsorted(unsorted: UnsortedWords) -> Words {
        let UnsortedWords {
            blob: unsorted_blob,
            mut words,
        } = unsorted;

        words.sort_by(|a, b| compare_plain_words(&unsorted_blob, a, b));

        let (_offset, _id, words) = words.iter().fold(
            // Accumulator is a tuple of (offset, id, Words)
            (
                0,
                0,
                Words {
                    blob: String::new(),
                    words: Vec::new(),
                },
            ),
            |acc, word| {
                let (
                    offset,
                    id,
                    Words {
                        mut blob,
                        mut words,
                    },
                ) = acc;

                blob.push_str(word.as_str(&unsorted_blob));
                words.push(Word::new(id, offset, word.len, word.score));

                (offset + word.len, id + 1, Words { blob, words })
            },
        );

        words
    }
}

#[derive(Debug, PartialEq)]
struct WordRange {
    start: usize,
    num_words: usize,
}

/// Span of words with the same length within a Words array.
#[derive(Debug, PartialEq)]
struct WordsOfLength {
    word_len: u8,
    range: Option<WordRange>,
}

#[derive(Debug, PartialEq)]
struct ByLength {
    max_length: u8,
    by_length: Vec<WordsOfLength>,
}

enum ByLengthState {
    Start,
    RunLength {
        word_len: u8,
        range_start: usize,
    }
}

impl ByLength {
    fn from_words(words: &Words) -> ByLength {
        if words.words.len() == 0 {
            return ByLength {
                max_length: 0,
                by_length: Vec::new(),
            }
        }

        let max_length = words.words.last().unwrap().len;

        let mut state = ByLengthState::Start;
        let mut by_length = Vec::new();
        let mut idx = 0;

        for (i, word) in words.words.iter().enumerate() {
            idx = i;

            match state {
                ByLengthState::Start => {
                    if word.len > 1 {
                        let mut last_len = 1;
                        while last_len < word.len {
                            let wol = WordsOfLength {
                                word_len: last_len,
                                range: None,
                            };
                            by_length.push(wol);

                            last_len += 1;
                        }
                    }

                    state = ByLengthState::RunLength {
                        word_len: word.len,
                        range_start: idx,
                    };
                }

                ByLengthState::RunLength { word_len, range_start } => {
                    if word.len == word_len {
                        state = ByLengthState::RunLength { word_len, range_start };
                    } else {
                        let wol = WordsOfLength {
                            word_len,
                            range: Some(WordRange { start: range_start, num_words: idx - range_start }),
                        };
                        by_length.push(wol);

                        if word.len > word_len + 1 {
                            let mut last_len = word_len + 1;
                            while last_len < word.len {
                                let wol = WordsOfLength {
                                    word_len: last_len,
                                    range: None,
                                };
                                by_length.push(wol);

                                last_len += 1;
                            }
                        }

                        state = ByLengthState::RunLength {
                            word_len: word.len,
                            range_start: idx,
                        };
                    }
                }
            }
        }

        idx += 1;
        if let ByLengthState::RunLength { word_len, range_start } = state {
            let wol = WordsOfLength {
                word_len,
                range: Some(WordRange { start: range_start, num_words: idx - range_start }),
            };
            by_length.push(wol);

        } else {
            unreachable!("should have gotten out of the ByLengthState::Start state");
        }

        ByLength {
            max_length,
            by_length,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn detects_invalid_lines() {
        assert_eq!(parse_line(""), Err(LineParseError::Invalid));
        assert_eq!(parse_line("x"), Err(LineParseError::Invalid));
        assert_eq!(parse_line("42"), Err(LineParseError::Invalid));
        assert_eq!(parse_line("FOO"), Err(LineParseError::Invalid));
        assert_eq!(parse_line(";"), Err(LineParseError::Invalid));
        assert_eq!(parse_line(";42"), Err(LineParseError::Invalid));
        assert_eq!(
            parse_line("FOOx;42"),
            Err(LineParseError::DisallowedChar { position: 3 })
        );
        assert_eq!(parse_line("FOO;"), Err(LineParseError::InvalidScore));
        assert_eq!(parse_line("FOO;BAR"), Err(LineParseError::InvalidScore));
        assert_eq!(parse_line("FOO;1234"), Err(LineParseError::InvalidScore));
        assert_eq!(parse_line("FOO;-1"), Err(LineParseError::InvalidScore));
        assert_eq!(parse_line("FOO;101"), Err(LineParseError::InvalidScore));
        assert_eq!(parse_line("FOO;10x"), Err(LineParseError::InvalidScore));
        assert_eq!(parse_line("FOO;10;"), Err(LineParseError::Invalid));
        assert_eq!(parse_line("FOO;10;BAR"), Err(LineParseError::Invalid));
    }

    #[test]
    fn parses_valid_lines() {
        assert_eq!(
            parse_line("FOO;42").unwrap(),
            Line {
                word: "FOO",
                score: 42
            }
        );
        assert_eq!(
            parse_line("ENTOMOLOGICAL;100").unwrap(),
            Line {
                word: "ENTOMOLOGICAL",
                score: 100
            }
        );
    }

    #[test]
    fn parses_unsorted_words() {
        let blob = r"HELLO;42
WORLD;43
FOO;44
";
        assert_eq!(
            UnsortedWords::from_blob(blob),
            UnsortedWords {
                blob: "HELLOWORLDFOO".to_string(),
                words: vec![
                    PlainWord {
                        start: 0,
                        len: 5,
                        score: 42,
                    },
                    PlainWord {
                        start: 5,
                        len: 5,
                        score: 43,
                    },
                    PlainWord {
                        start: 10,
                        len: 3,
                        score: 44,
                    }
                ],
            }
        );
    }

    #[test]
    fn parses_unsorted_words_and_skips_invalid_lines() {
        let blob = r"HELLO;42
WORLD;43
1FOO;44
";
        assert_eq!(
            UnsortedWords::from_blob(blob),
            UnsortedWords {
                blob: "HELLOWORLD".to_string(),
                words: vec![
                    PlainWord {
                        start: 0,
                        len: 5,
                        score: 42,
                    },
                    PlainWord {
                        start: 5,
                        len: 5,
                        score: 43,
                    },
                ],
            }
        );
    }

    #[test]
    fn creates_words() {
        let blob = r"HELLO;42
WORLD;43
FOO;44
";

        assert_eq!(
            Words::from_unsorted(UnsortedWords::from_blob(&blob)),
            Words {
                blob: "FOOHELLOWORLD".to_string(),
                words: vec![
                    Word {
                        id: 0,
                        start: 0,
                        len: 3,
                        score: 44,
                    },
                    Word {
                        id: 1,
                        start: 3,
                        len: 5,
                        score: 42,
                    },
                    Word {
                        id: 2,
                        start: 8,
                        len: 5,
                        score: 43,
                    },
                ],
            }
        );
    }

    #[test]
    fn creates_words2() {
        let blob = r"A;42
AA;44
B;43
AB;45
";

        assert_eq!(
            Words::from_unsorted(UnsortedWords::from_blob(&blob)),
            Words {
                blob: "ABAAAB".to_string(),
                words: vec![
                    Word {
                        id: 0,
                        start: 0,
                        len: 1,
                        score: 42,
                    },
                    Word {
                        id: 1,
                        start: 1,
                        len: 1,
                        score: 43,
                    },
                    Word {
                        id: 2,
                        start: 2,
                        len: 2,
                        score: 44,
                    },
                    Word {
                        id: 3,
                        start: 4,
                        len: 2,
                        score: 45,
                    },
                ],
            }
        );
    }

    #[test]
    fn creates_big_wordlist() {
        let _ = Words::from_unsorted(UnsortedWords::from_blob(&BLOB));
    }

    #[test]
    fn by_length_1() {
        let blob = "A;1\n";
        assert_eq!(
            ByLength::from_words(&Words::from_unsorted(UnsortedWords::from_blob(&blob))),
            ByLength {
                max_length: 1,
                by_length: vec![
                    WordsOfLength {
                        word_len: 1,
                        range: Some(WordRange { start: 0, num_words: 1 }),
                    }
                ],
            }
        );
    }

    #[test]
    fn by_length_2() {
        let blob = "AA;1\n";
        assert_eq!(
            ByLength::from_words(&Words::from_unsorted(UnsortedWords::from_blob(&blob))),
            ByLength {
                max_length: 2,
                by_length: vec![
                    WordsOfLength {
                        word_len: 1,
                        range: None,
                    },
                    WordsOfLength {
                        word_len: 2,
                        range: Some(WordRange { start: 0, num_words: 1 }),
                    }
                ],
            }
        );
    }

    #[test]
    fn by_length_3() {
        let blob = r"A;1
B;1";
        assert_eq!(
            ByLength::from_words(&Words::from_unsorted(UnsortedWords::from_blob(&blob))),
            ByLength {
                max_length: 1,
                by_length: vec![
                    WordsOfLength {
                        word_len: 1,
                        range: Some(WordRange { start: 0, num_words: 2 }),
                    }
                ],
            }
        );
    }

    #[test]
    fn by_length_4() {
        let blob = r"A;1
BBBB;1
CCCC;1
DDDDD;1";

        assert_eq!(
            ByLength::from_words(&Words::from_unsorted(UnsortedWords::from_blob(&blob))),
            ByLength {
                max_length: 5,
                by_length: vec![
                    WordsOfLength {
                        word_len: 1,
                        range: Some(WordRange { start: 0, num_words: 1 }),
                    },
                    WordsOfLength {
                        word_len: 2,
                        range: None,
                    },
                    WordsOfLength {
                        word_len: 3,
                        range: None,
                    },
                    WordsOfLength {
                        word_len: 4,
                        range: Some(WordRange { start: 1, num_words: 2 }),
                    },
                    WordsOfLength {
                        word_len: 5,
                        range: Some(WordRange { start: 3, num_words: 1 }),
                    }
                ],
            }
        );
    }

    #[test]
    fn by_length_5() {
        let blob = r"BBBB;1
DDDDDDD;1";

        assert_eq!(
            ByLength::from_words(&Words::from_unsorted(UnsortedWords::from_blob(&blob))),
            ByLength {
                max_length: 7,
                by_length: vec![
                    WordsOfLength {
                        word_len: 1,
                        range: None,
                    },
                    WordsOfLength {
                        word_len: 2,
                        range: None,
                    },
                    WordsOfLength {
                        word_len: 3,
                        range: None,
                    },
                    WordsOfLength {
                        word_len: 4,
                        range: Some(WordRange { start: 0, num_words: 1 }),
                    },
                    WordsOfLength {
                        word_len: 5,
                        range: None,
                    },
                    WordsOfLength {
                        word_len: 6,
                        range: None,
                    },
                    WordsOfLength {
                        word_len: 7,
                        range: Some(WordRange { start: 1, num_words: 1 }),
                    },
                ],
            }
        );
    }
}
